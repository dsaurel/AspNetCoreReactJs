Exemple d'application web DotnetCore/ReactJs développé avec Visual Studio Mac (Preview)

ReacJs

- webpack
- react-bootstrap
- react-redux
- redux-form
- react-dropzone

Asp.net Core 1.1

- FluenValition
- Automapper
- EntitFramework Core

Installation 

- git clone https://gitlab.com/dsaurel/AspNetCoreReactJs.git
- ouvrir AspNetCoreReactJs.sln avec Visual Studio Mac
- ouvrir un terminal 
-   npm install
-   npm run dev-watch (démarre webpack en mode watch)
- Démarrer le site en mode debug