﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using AspNetCoreReactJs.Services;

namespace AspNetCoreReactJs.Controllers
{
	/// <summary>
	/// File controller.
	/// </summary>
	[Route("api/[controller]")]
	public class FileController : Controller
	{

		/// <summary>
		/// The service.
		/// </summary>
		readonly IFileUploadService _service;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:AspNetCoreReactJs.Controllers.FileController"/> class.
		/// </summary>
		/// <param name="service">Service.</param>
		public FileController(IFileUploadService service)
		{
			_service = service;
		}

		/// <summary>
		/// Post the specified file.
		/// </summary>
		/// <returns>The post.</returns>
		/// <param name="file">File.</param>
		[HttpPost]
		public async Task<IActionResult> Post(IFormFile file)
		{
			if (file != null)
			{
				string url = await _service.Upload(file);
				if (url != null)
				{
					return Ok(url);
				}
			}

			return new NoContentResult();
		}
	}
}
