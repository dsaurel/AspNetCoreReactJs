namespace AspNetCoreReactJs.Controllers
{
	using System.Linq;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.Extensions.Logging;
	using Models;
	using Services;
	using Validators;
	using FluentValidation.Results;
	using System.Collections.Generic;
	using System;

	/// <summary>
	/// Partner controller.
	/// </summary>
	[Route("api/[controller]")]
    public class PartnerController : Controller
    {
		/// <summary>
		/// The service.
		/// </summary>
		readonly IService _service;

		/// <summary>
		/// The logger.
		/// </summary>
        readonly ILogger _logger;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:AspNetCoreReactJs.Controllers.PartnerController"/> class.
		/// </summary>
		/// <param name="service">Service.</param>
		/// <param name="logger">Logger.</param>
        public PartnerController(IService service, ILogger<PartnerController> logger){
            _service = service;
            _logger = logger;
        }

		/// <summary>
		/// Index this instance.
		/// </summary>
		/// <returns>The index.</returns>
        public IActionResult Index()
        {
            return View();
        }
        
		/// <summary>
		/// Get the specified id.
		/// </summary>
		/// <returns>The get.</returns>
		/// <param name="id">Identifier.</param>
        [HttpGet]
        public IActionResult Get(int? id){

			if(id == null){
				var model = PartnerViewModelFactoty.GetViewModel(
                    _service.GetWarehouseList().ToList(),
                    _service.GetHttpMethodList().ToList(), 
                    _service.GetSecurityModeList().ToList());
				
                return new JsonResult(model);
            }

			return null;
        }
 
		/// <summary>
		/// Post the specified model.
		/// </summary>
		/// <returns>The post.</returns>
		/// <param name="model">Model.</param>
        [HttpPost]
        public IActionResult  Post([FromBody] PartnerViewModel model){
            
			var validator = new PartnerViewModelValidator(_service);
			var results = validator.Validate(model);

            if(!results.IsValid){
                return BadRequest(results.Errors);
            }

			var entity = PartnerViewModelFactoty.GetEntity(
				model);

			_service.AddPartner(entity);

			return Ok();
        }
    }
}
