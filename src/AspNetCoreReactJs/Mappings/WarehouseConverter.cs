﻿using System;
using System.Linq;
using AspNetCoreReactJs.Entities;
using AspNetCoreReactJs.Models;
using AutoMapper;
using System.Collections.Generic;

namespace AspNetCoreReactJs
{
	/// <summary>
	/// Warehouse converter.
	/// </summary>
	public class WarehouseConverter: ITypeConverter<WarehouseViewModel,Warehouse>
	{
		/// <summary>
		/// Convert the specified source, destination and context.
		/// </summary>
		/// <returns>The convert.</returns>
		/// <param name="source">Source.</param>
		/// <param name="destination">Destination.</param>
		/// <param name="context">Context.</param>
		public Warehouse Convert(WarehouseViewModel source, Warehouse destination, ResolutionContext context)
		{
			Warehouse warehouse = new Warehouse();
			warehouse.WarehouseId = source.WarehouseId;
			warehouse.WarehouseName = source.WarehouseName;
			warehouse.SupplierOnly = source.SupplierOnly;
			warehouse.SupplierOnlyChecked = source.SupplierOnlyChecked;
			warehouse.CategoryList = new List<Category>();

			if (!source.CategoryIdList.Contains(0)){

				source.CategoryIdList.ForEach(categoryId =>
				{
					SelectModelView model = source.CategoryList.FirstOrDefault(c => c.Value == categoryId);
					if (model != null)
					{
						warehouse.CategoryList.Add(new Category
						{
							CategoryId = model.Value,
							CategoryName = model.Text
						});
					}
				});
			}

			return warehouse;
		}
	}
}
