﻿using AspNetCoreReactJs.Entities;
using AspNetCoreReactJs.Models;
using AutoMapper;

namespace AspNetCoreReactJs.Mappings
{
	/// <summary>
	/// Auto mapper profile.
	/// </summary>
	public class AutoMapperProfile : Profile
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="T:AspNetCoreReactJs.Mappings.AutoMapperProfile"/> class.
		/// </summary>
		public AutoMapperProfile()
		{
			CreateMap<PartnerViewModel, Partner>();

			CreateMap<WarehouseViewModel, Warehouse>().ConvertUsing(new WarehouseConverter());
		}
	}
}