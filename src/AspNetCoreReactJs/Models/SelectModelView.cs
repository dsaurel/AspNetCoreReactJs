namespace AspNetCoreReactJs.Models{

    public class SelectModelView{

        public int Value {get; set;}

        public string Text {get; set;}

    }

}