using System.Collections.Generic;
using System.Linq;
using System;
using AutoMapper;
using AspNetCoreReactJs.Entities;

namespace AspNetCoreReactJs.Models{

    
	/// <summary>
	/// Partner view model factoty.
	/// </summary>
    public static class PartnerViewModelFactoty{

		/// <summary>
		/// Create the specified warehouseList, httpMethodList and securityModeList.
		/// </summary>
		/// <returns>The create.</returns>
		/// <param name="warehouseList">Warehouse list.</param>
		/// <param name="httpMethodList">Http method list.</param>
		/// <param name="securityModeList">Security mode list.</param>
        public static PartnerViewModel GetViewModel(List<Warehouse> warehouseList, List<HttpMethod> httpMethodList, List<SecurityMode> securityModeList){
            var partner = new PartnerViewModel();
            partner.PartnerId = 0;
			partner.PartnerToken = Guid.NewGuid().ToString();
            partner.WarehouseList = new List<WarehouseViewModel>();

            partner.HttpMethodList = new List<SelectModelView>();
            partner.SecurityModeList = new List<SelectModelView>();

            securityModeList.ForEach(entity => {
				partner.SecurityModeList.Add(new SelectModelView{Value = entity.SecurityModeId, Text = entity.SecurityModeLabel});
            });

            httpMethodList.ForEach(entity => {
                partner.HttpMethodList.Add(new SelectModelView{Value = entity.HttpMethodId, Text = entity.HttpMethodLabel});
            });

            partner.SelectedHttpMethod = httpMethodList.FirstOrDefault().HttpMethodId;
            partner.SelectedSecurityMode = securityModeList.FirstOrDefault().SecurityModeId;

            warehouseList.ForEach(warehouse => {

                var warehouseModel = new WarehouseViewModel{};
                warehouseModel.WarehouseId = warehouse.WarehouseId;
                warehouseModel.WarehouseName = warehouse.WarehouseName;

                warehouseModel.CategoryList = new List<SelectModelView>();
                warehouseModel.CategoryIdList = new List<int>();

				if (warehouse.WarehouseId == 1 || warehouse.WarehouseId == 2)
				{
					warehouseModel.SupplierOnly = true;
				}

				warehouseModel.SupplierOnlyChecked = false;

				warehouseModel.CategoryList.Add(new SelectModelView { Value = 0, Text = "Toutes les catégories" });

				warehouse.CategoryList.ToList().ForEach(category => {
					warehouseModel.CategoryList.Add(new SelectModelView {Value = category.CategoryId, Text = category.CategoryName });
				});

				warehouseModel.CategoryIdList = new List<int> {0};

				partner.WarehouseList.Add(warehouseModel);

            });

            return partner;
        }

		/// <summary>
		/// Creates the entity.
		/// </summary>
		/// <returns>The entity.</returns>
		/// <param name="viewModel">View model.</param>
		/// <param name="warehouseList">Warehouse list.</param>
		/// <param name="httpMethodList">Http method list.</param>
		/// <param name="securityModeList">Security mode list.</param>
		public static Partner GetEntity(PartnerViewModel viewModel)
		{
			Partner partnerEntity = Mapper.Map<PartnerViewModel, Partner>(viewModel);

			partnerEntity.Warehouses = new List<Warehouse>();

			viewModel.WarehouseList.Where(warehouse => warehouse.Checked).ToList().ForEach(warehouse =>
			{
				Warehouse warehouseEntity = Mapper.Map<WarehouseViewModel, Warehouse>(warehouse);
				partnerEntity.Warehouses.Add(warehouseEntity);
			});

			return partnerEntity;
		}
    }
}