

namespace AspNetCoreReactJs.Models
{
    using System.Collections.Generic;

    public class PartnerViewModel
    {
        public int PartnerId { get; set; }

        public string PartnerName { get; set; }

        public bool Enabled { get; set; }

        public bool UseCatalogPrice { get; set; }
        
        public string SakuraToken { get; set; }

        public string PartnerToken {get; set;}

        public string LogoUrl { get; set; }

        public bool PrestashopWired { get; set; }

        public string PrestashopModuleUrl { get; set; }

        public int SelectedSecurityMode { get; set; }

        public int SelectedHttpMethod { get; set; }

        public List<SelectModelView> SecurityModeList { get; set; }

        public List<SelectModelView> HttpMethodList { get; set; }

        public List<WarehouseViewModel> WarehouseList { get; set; }
    }
}