using System.Collections.Generic;

namespace AspNetCoreReactJs.Models
{
    public class WarehouseViewModel
    {
        public int WarehouseId { get; set; }

        public string WarehouseName { get; set; }

        public bool Checked { get; set;}

		public bool SupplierOnly { get; set; }

		public bool SupplierOnlyChecked { get; set; }

        public List<SelectModelView> CategoryList {get; set;}

        public List<int> CategoryIdList { get; set; }


    }
}