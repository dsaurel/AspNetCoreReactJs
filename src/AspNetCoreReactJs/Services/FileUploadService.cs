﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.Extensions.Logging;

namespace AspNetCoreReactJs.Services
{
	/// <summary>
	/// File upload service.
	/// </summary>
	public class FileUploadService : IFileUploadService
	{
		/// <summary>
		/// The context.
		/// </summary>
		readonly IHttpContextAccessor _context;

		/// <summary>
		/// The environment.
		/// </summary>
		readonly IHostingEnvironment _environment;

		/// <summary>
		/// The logger.
		/// </summary>
		readonly ILogger<FileUploadService> _logger;

		/// <summary>
		/// The upload directory.
		/// </summary>
		const string UploadDirectory = "uploads";

		/// <summary>
		/// Initializes a new instance of the <see cref="T:AspNetCoreReactJs.Services.FileUploadService"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="environment">Environment.</param>
		/// <param name="logger">Logger.</param>
		public FileUploadService(IHttpContextAccessor context, IHostingEnvironment environment, ILogger<FileUploadService> logger)
		{
			_context = context;
			_environment = environment;
			_logger = logger;
		}

		/// <summary>
		/// Gets the base URL.
		/// </summary>
		/// <returns>The base URL.</returns>
		public string GetBaseUrl()
		{
			var request = _context.HttpContext.Request;

			var host = request.Host.ToUriComponent();

			var pathBase = request.PathBase.ToUriComponent();

			var baseUrl = $"{request.Scheme}://{host}{pathBase}";

			return baseUrl;
		}

		/// <summary>
		/// Gets the file URL.
		/// </summary>
		/// <returns>The file URL.</returns>
		/// <param name="fileName">File name.</param>
		public string GetFileUrl(string fileName)
		{
			return GetBaseUrl() + $"/{UploadDirectory}/{fileName}";
		}

		/// <summary>
		/// Gets the upload directory path.
		/// </summary>
		/// <returns>The upload directory path.</returns>
		public string GetUploadDirectoryPath()
		{
			return Path.Combine(_environment.WebRootPath, UploadDirectory);
		}

		/// <summary>
		/// Gets the file path.
		/// </summary>
		/// <returns>The file path.</returns>
		/// <param name="fileName">File name.</param>
		public string GetFilePath(string fileName)
		{
			return Path.Combine(GetUploadDirectoryPath(), fileName);
		}

		/// <summary>
		/// Upload the specified file.
		/// </summary>
		/// <returns>The upload.</returns>
		/// <param name="file">File.</param>
		public async Task<string> Upload(IFormFile file)
		{
			try
			{
				var baseUrl = GetBaseUrl();

				var uploads = GetUploadDirectoryPath();
				if (!Directory.Exists(uploads))
				{
					Directory.CreateDirectory(uploads);
				}

				var fileName = Guid.NewGuid().ToString();

				var info = new FileInfo(file.FileName);
				if (!string.IsNullOrWhiteSpace(info.Extension))
				{
					fileName += info.Extension;
				}
				using (var fileStream = new FileStream(GetFilePath(fileName), FileMode.Create))
				{
					await file.CopyToAsync(fileStream);
				}

				return GetFileUrl(fileName);
			}
			catch (Exception ee)
			{
				_logger.LogError(ee.Message);
				return null;
			}
		}
	}
}
