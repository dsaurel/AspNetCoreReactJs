﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace AspNetCoreReactJs.Services
{
	/// <summary>
	/// File upload service.
	/// </summary>
	public interface IFileUploadService
	{
		/// <summary>
		/// Upload the specified form.
		/// </summary>
		/// <returns>The upload.</returns>
		/// <param name="form">Form.</param>
		Task<string> Upload(IFormFile form);
	}
}