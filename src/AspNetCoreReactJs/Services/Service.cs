namespace AspNetCoreReactJs.Services
{
	using System.Collections.Generic;
	using Entities;
	using Microsoft.EntityFrameworkCore;
	using System.Linq;
	using System;

	/// <summary>
	/// Service.
	/// </summary>
	public class Service:IService {

		/// <summary>
		/// The context.
		/// </summary>
        readonly Context _context;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:AspNetCoreReactJs.Services.Service"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
        public Service(Context context){
            _context = context;
        }

		/// <summary>
		/// Gets the category list.
		/// </summary>
		/// <returns>The category list.</returns>
        public IEnumerable<Category> GetCategoryList(){
            return _context.Category;
        }

		/// <summary>
		/// Gets the warehouse list.
		/// </summary>
		/// <returns>The warehouse list.</returns>
        public IEnumerable<Warehouse> GetWarehouseList(){
			return _context.Warehouse.Include(warehouse => warehouse.CategoryList);
        }

		/// <summary>
		/// Gets the http method list.
		/// </summary>
		/// <returns>The http method list.</returns>
        public IEnumerable<HttpMethod> GetHttpMethodList()
        {
            return _context.HttpMethod;
        }

		/// <summary>
		/// Gets the security mode list.
		/// </summary>
		/// <returns>The security mode list.</returns>
        public IEnumerable<SecurityMode> GetSecurityModeList()
        {
            return _context.SecurityMode;
        }

		/// <summary>
		/// Partners the exist.
		/// </summary>
		/// <returns><c>true</c>, if exist was partnered, <c>false</c> otherwise.</returns>
		/// <param name="partnerName">Partner name.</param>
		public bool PartnerExist(string partnerName)
		{
			if (string.IsNullOrWhiteSpace(partnerName)){
				return false;
			}

			return _context.Partner.FirstOrDefault(c => c.PartnerName == partnerName.Trim()) != null;
		}

		/// <summary>
		/// Adds the partner.
		/// </summary>
		/// <param name="partner">Partner.</param>
		public void AddPartner(Partner partner)
		{
			partner.Warehouses.ToList().ForEach(warehouse => {
				_context.Warehouse.Attach(warehouse);

				warehouse.CategoryList.ToList().ForEach(category =>
				{
					_context.Category.Attach(category);
				});
			});

			_context.Partner.Add(partner);
			_context.SaveChanges();
		}
	}

}