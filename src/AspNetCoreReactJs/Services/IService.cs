namespace AspNetCoreReactJs.Services{

    using Entities;
    using System.Collections.Generic;

	/// <summary>
	/// Service.
	/// </summary>
    public interface IService {

		/// <summary>
		/// Gets the category list.
		/// </summary>
		/// <returns>The category list.</returns>
        IEnumerable<Category> GetCategoryList();

		/// <summary>
		/// Gets the warehouse list.
		/// </summary>
		/// <returns>The warehouse list.</returns>
        IEnumerable<Warehouse> GetWarehouseList();

		/// <summary>
		/// Gets the security mode list.
		/// </summary>
		/// <returns>The security mode list.</returns>
		IEnumerable<SecurityMode> GetSecurityModeList();

		/// <summary>
		/// Gets the http method list.
		/// </summary>
		/// <returns>The http method list.</returns>
        IEnumerable<HttpMethod> GetHttpMethodList();

		/// <summary>
		/// Partners the exist.
		/// </summary>
		/// <returns><c>true</c>, if exist was partnered, <c>false</c> otherwise.</returns>
		/// <param name="partnerName">Partner name.</param>
		bool PartnerExist(string partnerName);

		/// <summary>
		/// Adds the partner.
		/// </summary>
		/// <param name="partner">Partner.</param>
		void AddPartner(Partner partner);
    }

}