﻿import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Panel, FormGroup, FormControl, ControlLabel, Checkbox, Button, ButtonToolbar, Image,Thumbnail, FieldGroup, Grid, Row,Col} from 'react-bootstrap';
import { Field, FieldArray, Fields } from 'redux-form';
import Dropzone from 'react-dropzone';
import Griddle from 'griddle-react';

const renderTextField = (field) => (
    <FormGroup validationState={field.meta.error && "error"}>
      <ControlLabel>{field.placeholder}</ControlLabel>
      <FormControl {...field.input} type="text" placeholder={field.placeholder}/>
      {field.meta.touched && field.meta.error && 
       <ControlLabel>{field.meta.error}</ControlLabel>}
    </FormGroup>
)

const renderCheckBoxField = (field) => (
   <FormGroup validationState={field.meta.error && "error"}>
      <ControlLabel>{field.placeholder}</ControlLabel>
      <Checkbox {...field.input}></Checkbox>
      {field.meta.touched && field.meta.error && 
       <ControlLabel>{field.meta.error}</ControlLabel>}
   </FormGroup>
)

const renderSecurityModeSelect = (fields) => {

	return (
		<FormGroup>
			<ControlLabel>{fields.placeholder}</ControlLabel>
			<FormControl componentClass="select" placeholder={fields.placeholder} {...fields.SelectedSecurityMode.input}>

			{Array.isArray(fields.SecurityModeList.input.value) && 
			fields.SecurityModeList.input.value.map((option) => {
				return <option key= {option.Value} value={option.Value}>{option.Text}</option>
			})}

			</FormControl>
		</FormGroup>
	)
}

const renderHttpMethodSelect = (fields) => {

	return (
		<FormGroup>
			<ControlLabel>{fields.placeholder}</ControlLabel>
			<FormControl componentClass="select" placeholder={fields.placeholder} {...fields.SelectedHttpMethod.input}>

			{Array.isArray(fields.HttpMethodList.input.value) && 
			fields.HttpMethodList.input.value.map((option) => {
				return <option key= {option.Value} value={option.Value}>{option.Text}</option>
			})}

			</FormControl>
		</FormGroup>
	)
}

const renderCategorySelectMultiple = (fields) => {

	const message = 'Dont je suis le fournisseur';

	let warehouse = fields.WarehouseList[fields.index];

	let checked = false;

	let supplierOnly;

	let supplierOnlyChecked;

	let categoryList;
	let categoryIdList;

	if(warehouse !== null && 
	   warehouse.CategoryList !== null &&
	   warehouse.CategoryList.input !== null &&
	   warehouse.CategoryList.input.value !== null &&
	   Array.isArray(warehouse.CategoryList.input.value)
	   ){

		categoryList = warehouse.CategoryList.input.value;
		categoryIdList = warehouse.CategoryIdList;
		checked = warehouse.Checked.input.value;
		supplierOnly = warehouse.SupplierOnly.input.value;
		supplierOnlyChecked = warehouse.SupplierOnlyChecked.input.value;
	}

	return (

		<Row>
			<Col md={4}>
        		<Field name={"WarehouseList["+fields.index+"].Checked"} component="input" type="Checkbox"/> {warehouse.WarehouseName.input.value}
    		</Col>
    		<Col md={4}>
				{checked && categoryList 
				&&
				<FormControl componentClass="select" multiple={true} placeholder={fields.placeholder} {...categoryIdList.input}>

				{categoryList && categoryList.map((category) => {
					return <option key= {category.Value} value={category.Value}>{category.Text}</option>
				})}
				</FormControl>}
			</Col>
			<Col md={4}>
			{supplierOnly && checked &&
				<Field name={"WarehouseList["+fields.index+"].SupplierOnlyChecked"} component="input" type="Checkbox"/>
			}
			</Col>

		</Row>
		
	)
}

const renderDropzoneField = (field) => {

  const uploaded = field.input.value;

  const onDrop = (acceptedFiles, rejectedFiles) =>{
    var data = new FormData();
  	data.append('file',acceptedFiles[0]);
	field.trigger(data);
  }

  return (
   	  <FormGroup validationState={field.meta.error && "error"}>
   	  <ControlLabel>{field.placeholder}</ControlLabel>
      <Dropzone name={field.name} onDrop={( files, e ) => onDrop(files)}>
      </Dropzone>
      {field.meta.touched && field.meta.error && 
       <ControlLabel>{field.meta.error}</ControlLabel>} 
    </FormGroup>
  );
}

const renderWarehouseList = ({fields}) => {
    return (
        <Panel header="Unités de production">
            <Grid>
                {fields.map((warehouse,index) =>
                	<Fields key={index} names={
	            	[`${warehouse}.Checked`,
	            	`${warehouse}.WarehouseName`,
	            	`${warehouse}.CategoryList`,
	            	`${warehouse}.CategoryIdList`,
	            	`${warehouse}.SupplierOnly`,
	            	`${warehouse}.SupplierOnlyChecked`
	            	]
	            	} index={index} component={renderCategorySelectMultiple}/>
                )}
            </Grid>
        </Panel>    
    )
}

class Partner extends Component
{
  componentDidMount(){ 
      this.props.fetchNewPartner();
  }

  render(){

  	let fakeData = [];

  	for(let i = 0;i<100;i++){
  		fakeData.push({
	    "id": i,
	    "Partner": "Partenaire "+i,
	    "city": "Kapowsin",
	    "state": "Hawaii",
	    "country": "United Kingdom",
	    "company": "Ovolo",
	    "favoriteNumber": 7
	  });
  	}


    return (

            <div>

            	<Griddle results={fakeData} />

                <Panel header="Informations générales">
                	<Field name="Enabled" placeholder="Activé" component={renderCheckBoxField}/>

                    <Field name="PartnerName" placeholder="Nom du partenaire" component={renderTextField}/>

                    <Grid>
        				<Row>
		    				<Col xs={6} md={4}>
			                	<Field name="LogoUrl" placeholder="Logo du partenaire" trigger={this.props.changeLogoFile} component={renderDropzoneField}/>
			                </Col>
			                <Col xs={6} md={4}>
			                   	{this.props.logoUrl && <Image href="#" src={this.props.logoUrl} responsive/>}
			                </Col>
            			</Row>
            		</Grid>

                </Panel>

                <Panel header="API de prise de commande">
            		<Field name="PrestashopWired" placeholder="Connecté à un Prestashop" component={renderCheckBoxField}/>

            		{this.props.isPrestashopWired && <Field name="PrestashopModuleUrl" placeholder="Url du module Prestashop" component={renderTextField}/>}

            		<Field name="UseCatalogPrice" placeholder="Utiliser les prix du catalogue" component={renderCheckBoxField}/>

                </Panel>

                <Panel header="Sakura">
                    <Field name="SakuraToken" placeholder="Token Sakura" component={renderTextField}/>
                </Panel>

                <Panel header="Sécurisation des images des clients">

                	<Fields names={['SelectedSecurityMode','SecurityModeList']} placeholder="Type de sécurité" component={renderSecurityModeSelect} />

                	<Fields names={['SelectedHttpMethod','HttpMethodList']} placeholder="Méthode HTTP" component={renderHttpMethodSelect} />

                </Panel>

                <FieldArray name="WarehouseList" component={renderWarehouseList}/>

                <Button bsStyle="primary" onClick={this.props.postPartner} disabled={this.props.submitting}>Enregistrer</Button>
            </div>
        );
    }
}

export default Partner;