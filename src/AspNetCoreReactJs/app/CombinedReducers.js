﻿import {combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

const CombinedReducers = combineReducers(
	{form:formReducer}
); 

export default CombinedReducers;