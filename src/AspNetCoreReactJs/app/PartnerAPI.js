import Constants from './Constants';

const API_HEADERS = {
  'Content-Type': 'application/json'
}

function checkStatus(response) {
  if (response.status === 200) {
    return response
  } else {
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
}

let PartnerApi = {
    fetchNewPartner() {
        return fetch(Constants.PARTNER_API_URL,{headers:API_HEADERS})
        .then((response) => response.json());
    }, 

    postPartner(partner) {
	    return fetch(Constants.PARTNER_API_URL, {
	      method: 'POST',
	      headers: API_HEADERS,
	      body: JSON.stringify(partner)
	    })
    },

    postFile(file){
    	return fetch(Constants.FILE_API_URL, {
	      method: 'POST',
	      body: file
	    })
	    .then(checkStatus)
	    .then((response) => response.text());
    }
};
export default PartnerApi;