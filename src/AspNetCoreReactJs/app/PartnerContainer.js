import { connect } from 'react-redux';
import Partner from './Partner';
import PartnerCreators from './PartnerCreators';
import {reduxForm,formValueSelector } from 'redux-form';

const selector = formValueSelector('Partner')

const mapStoreToProps = (state) =>{
	return {
    	logoUrl: selector(state, 'LogoUrl'),
    	isPrestashopWired: selector(state, 'PrestashopWired')
  	}
};

const mapDispatchToProps = (dispatch) =>{
  return {
    fetchNewPartner:()=>dispatch(PartnerCreators.fetchNewPartner()),
    postPartner:()=>dispatch(PartnerCreators.postPartner()),
    changeLogoFile:(file)=>dispatch(PartnerCreators.changeLogoFile(file)),
  }
}

const partner = reduxForm(
  {form: 'Partner', onSubmit: PartnerCreators.submit}  // a unique identifier for this form
)(Partner)

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(partner);

