import constants from './Constants.js';
import PartnerApi from './PartnerApi';
import {initialize,change,submit,SubmissionError} from 'redux-form';

const createSubmissionError = (errors) => {

	let submissionErrors = {};

	errors.map((error) => {
		submissionErrors[error.PropertyName] = error.ErrorMessage;
	});

	console.log(submissionErrors);

	return new SubmissionError(submissionErrors);
}

const PartnerCreators = {

    fetchNewPartner() {
        return (dispatch) => {
            PartnerApi.fetchNewPartner().then(
                (partner) => dispatch(initialize('Partner',partner)), 
                (error) => dispatch({ type: constants.NEW_PARTNER_GET_RESPONSE, success:false })
            ); 
        };
    },

    postPartner() {
        return (dispatch) => {
        	dispatch(submit('Partner'))
		};
	},
	changeLogoFile(file){
		return (dispatch) => {
		     	
        	PartnerApi.postFile(file).then(
                (logoUrl) => dispatch(change('Partner','LogoUrl',logoUrl)),
                (error) => dispatch({ type: constants.LOGO_FILE_POST_RESPONSE, success:false, error })
        	);
		};
	},

	submit(values){
    	return new Promise((resolve, reject) => {
		    return PartnerApi.postPartner(values)
		      .then((response) => {
		        if(response.ok){
		        	resolve();
		        }
		        else{
		        	return response.json();	
		        }
		      })
		      .then((response) => {
		      	reject(createSubmissionError(response));
		      })
		      .catch((response) => {
		        reject(new SubmissionError({ _error: response.body}));
		      });
	  });
	}
}

export default PartnerCreators;