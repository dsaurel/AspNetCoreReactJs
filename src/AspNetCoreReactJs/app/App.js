import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import Store from "./Store";
import PartnerContainer from './PartnerContainer';
import WarehouseListContainer from './WarehouseListContainer';

ReactDOM.render(
     <Provider store={Store}>
        <PartnerContainer />
    </Provider>
,document.getElementById('root'));

