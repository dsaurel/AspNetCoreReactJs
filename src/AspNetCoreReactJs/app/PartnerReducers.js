import constants from "./Constants";
import update from 'react-addons-update';

const initialState = {
	
}

const PartnerReducers = (state = initialState, action) => {

	switch(action.type){

        case constants.LOGO_FILE_POST_RESPONSE:
            return update(state,
                {
                    logoUrl: {
                        $set: action.logoUrl
                    }
                });
            break;

		case constants.NEW_PARTNER_GET_RESPONSE:
			return action.partner;

		 case constants.CHANGE_ENABLED:

		 	let newEnabledValue;

            return update(state,
            	{enabled: { $apply: enabled => {
                                      newEnabledValue = !enabled
                            return newEnabledValue; }}}
            		
            	);
          
		case constants.CHANGE_PARTNER_NAME:
            return update(state,
            {
        		partnerName: {
        			$set: action.partnerName
        		}
            });

        case constants.CHANGE_SAKURA_TOKEN:
        	return update(state,
            {
        		sakuraToken: {
        			$set: action.sakuraToken
        		}
            });

        case constants.CHANGE_PRESTASHOP_WIRED:
        	let newPrestashopWiredValue;

        	return update(state,
            	{prestashopWired: { $apply: prestashopWired => {
                                      newPrestashopWiredValue = !prestashopWired
                            return newPrestashopWiredValue; }}}
            		
            	);

    	case constants.CHANGE_PRESTASHOP_MODULE_URL:
        	return update(state,
            {
        		prestashopModuleUrl: {
        			$set: action.prestashopModuleUrl
        		}
            });

        case constants.CHANGE_USE_CATALOG_PRICE:
        	let newUseCatalogPriceValue;

        	return update(state,
            	{useCatalogPrice: { $apply: useCatalogPrice => {
                                      newUseCatalogPriceValue = !useCatalogPrice
                            return newUseCatalogPriceValue; }}}
            		
            	);

    	case constants.CHANGE_SECURITY_MODE:
        	return update(state,
            {
        		selectedSecurityMode: {
        			$set: parseInt(action.securityMode)
        		}
            });

        case constants.CHANGE_HTTP_METHOD:
        	return update(state,
            {
        		selectedHttpMethod: {
        			$set: parseInt(action.httpMethod)
        		}
            });

		case constants.LOGO_FILE_POST_RESPONSE:
			return update(state,
            {
        		logoUrl: {
        			$set: action.logoUrl
        		}
            });

    }

    if(action.warehouseId != undefined){

        let index = state.warehouseList.findIndex((warehouse) => warehouse.warehouseId === action.warehouseId);
	    if(index === -1){
	        return state;
	    }

	    switch (action.type) {
        case constants.CHANGE_WAREHOUSE_ENABLED:

            let newCheckedValue;

                return update(
                    state,
                    {warehouseList:
                        {[index]:
                            {checked: { $apply: checked => {
                                      newCheckedValue = !checked
                            return newCheckedValue; }}}
                        }
                    }
                );

        case constants.CHANGE_WAREHOUSE_SUPPLIER_ONLY:

            let newSupplierOnlyValue;

            return update(
                state,
                {warehouseList:
                    {[index]:
                        {supplierOnly: { $apply: supplierOnly => 
                            {newSupplierOnlyValue = !supplierOnly 
                        return newSupplierOnlyValue;}}}
                    }
                }
            );
       
        case constants.CHANGE_WAREHOUSE_CATEGORY:

            action.categoryIdList = action.categoryIdList.map((item) => item = parseInt(item));
            
            if(action.categoryIdList.indexOf(0) !== -1){
                action.categoryIdList = [0];
            }

           return update(
                state,
                {warehouseList:
                    {[index]:
                        {selectedCategoryIdList: { $set: action.categoryIdList}}
                    }
                }
            );
    }
    }

    return state;
};

export default PartnerReducers;