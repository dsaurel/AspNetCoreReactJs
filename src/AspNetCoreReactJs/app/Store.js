import { applyMiddleware, createStore } from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import CombinedReducers from './CombinedReducers';

const logger = createLogger();

const Store = createStore(
  CombinedReducers,
  applyMiddleware(logger,thunk)
);


export default Store;