namespace AspNetCoreReactJs.Entities
{

     using System.ComponentModel.DataAnnotations;

    public class HttpMethod{

        [Key]
        public int HttpMethodId {get; set;}

        public string HttpMethodLabel {get; set;}

    }
}