using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace AspNetCoreReactJs.Entities
{
    public class Partner {
        [Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int PartnerId { get; set; }

		public string PartnerName { get; set; }

		public bool Enabled { get; set; }

		public bool UseCatalogPrice { get; set; }

		public string SakuraToken { get; set; }

		public string PartnerToken { get; set; }

		public string LogoUrl { get; set; }

		public bool PrestashopWired { get; set; }

		public string PrestashopModuleUrl { get; set; }

		public int SelectedSecurityMode { get; set; }

		public int SelectedHttpMethod { get; set; }

		public virtual ICollection<Warehouse> Warehouses { get; set; }

    }
}