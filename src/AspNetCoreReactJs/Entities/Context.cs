namespace AspNetCoreReactJs.Entities
{

    using Microsoft.EntityFrameworkCore;

    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options)
            : base(options)
        {
            DbInitializer.Initialize(this);
        }
 
        public DbSet<Partner> Partner { get; set; }
 
        public DbSet<Warehouse> Warehouse { get; set; }

        public DbSet<Category> Category {get; set;}

        
        public DbSet<SecurityMode> SecurityMode {get; set;}

        public DbSet<HttpMethod> HttpMethod {get; set;}
    }

}