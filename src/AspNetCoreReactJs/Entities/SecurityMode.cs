namespace AspNetCoreReactJs.Entities{

     using System.ComponentModel.DataAnnotations;

    public class SecurityMode{

        [Key]
        public int SecurityModeId {get; set;}

        public string SecurityModeLabel {get; set;}

    }
}