namespace AspNetCoreReactJs.Entities{
	
  	using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Warehouse {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WarehouseId {get; set;}

        public string WarehouseName {get; set;}

        public bool SupplierOnly {get; set;}

		public bool SupplierOnlyChecked { get; set; }

        public virtual ICollection<Category> CategoryList {get; set;}


    }

}