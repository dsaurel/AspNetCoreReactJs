namespace AspNetCoreReactJs.Entities{

    using System.Collections.Generic;

    public class PartnerWarehouse {

        public Partner Partner {get; set;}

        public int PartnerId{get; set;}

        public Warehouse Warehouse {get; set;}

        public int WarehouseId {get; set;}

        public virtual ICollection<Category> CategoryList {get; set;}
    }
}