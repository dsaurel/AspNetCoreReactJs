namespace AspNetCoreReactJs.Entities
{
	using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Db initializer.
    /// </summary>
    public static class DbInitializer
    {
        /// <summary>
        /// Initialize the specified context.
        /// </summary>
        /// <returns>The initialize.</returns>
        /// <param name="context">Context.</param>
        public static void Initialize(Context context)
        {
            if (context.Warehouse.Any())
            {
                return;
            }

            var warehouse1 = new Warehouse {WarehouseId=1,WarehouseName="Usine de Pessac"};
            var warehouse2 = new Warehouse {WarehouseId=2,WarehouseName="Fablac Nice"};
            var warehouse3 = new Warehouse {WarehouseId=3,WarehouseName="Fablab Mérignac"};
            var warehouse4 = new Warehouse {WarehouseId=4,WarehouseName="Fablab Paris Les Ulis"};
            var warehouse5 = new Warehouse {WarehouseId=5,WarehouseName="Fablab Sarran"};
            
            context.SecurityMode.Add(new SecurityMode{SecurityModeId=1, SecurityModeLabel = "Pas de sécurisation"});
            context.SecurityMode.Add(new SecurityMode{SecurityModeId=2, SecurityModeLabel = "Token"});

            context.HttpMethod.Add(new HttpMethod{HttpMethodId=1, HttpMethodLabel="Post"});
            context.HttpMethod.Add(new HttpMethod{HttpMethodId=2, HttpMethodLabel="Get"});
            
            var category1 = new Category{CategoryId = 1, CategoryName="Coque"};
            var category2 = new Category{CategoryId = 2, CategoryName="Toile"};
            var category3 = new Category{CategoryId = 3, CategoryName="Plexi"};
            var category4 = new Category{CategoryId = 4, CategoryName="Mug"};
			var category5 = new Category { CategoryId = 5, CategoryName = "Magnet" };

			context.Category.AddRange(category1, category2, category3, category4, category5);

			warehouse1.CategoryList = new List<Category>();
			warehouse1.CategoryList.Add(category1);

			context.Warehouse.Add(warehouse1);

			warehouse2.CategoryList = new List<Category>();
			warehouse2.CategoryList.Add(category1);
			warehouse2.CategoryList.Add(category2);

			context.Warehouse.Add(warehouse2);

			warehouse3.CategoryList = new List<Category>();
			warehouse3.CategoryList.Add(category1);
			warehouse3.CategoryList.Add(category2);
			warehouse3.CategoryList.Add(category3);

			context.Warehouse.Add(warehouse3);

			warehouse4.CategoryList = new List<Category>();
			warehouse4.CategoryList.Add(category1);
			warehouse4.CategoryList.Add(category2);
			warehouse4.CategoryList.Add(category3);
			warehouse4.CategoryList.Add(category4);

			context.Warehouse.Add(warehouse4);

			warehouse5.CategoryList = new List<Category>();
			warehouse5.CategoryList.Add(category1);
			warehouse5.CategoryList.Add(category2);
			warehouse5.CategoryList.Add(category3);
			warehouse5.CategoryList.Add(category4);
			warehouse5.CategoryList.Add(category5);

			context.Warehouse.Add(warehouse5);

            context.SaveChanges();
        }
    }
}