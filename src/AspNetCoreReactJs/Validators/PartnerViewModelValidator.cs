namespace AspNetCoreReactJs.Validators{

	using Models;
	using FluentValidation;
	using AspNetCoreReactJs.Services;

	/// <summary>
	/// Partner view model validator.
	/// </summary>
	public class PartnerViewModelValidator:AbstractValidator<PartnerViewModel>{

		/// <summary>
		/// The service.
		/// </summary>
		private readonly IService _service;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:AspNetCoreReactJs.Validators.PartnerViewModelValidator"/> class.
		/// </summary>
		/// <param name="service">Service.</param>
        public PartnerViewModelValidator(IService service){

			_service = service;

            RuleFor(model => model.PartnerName).NotNull().WithMessage("Le nom du partenaire est requis");

			When(model => !string.IsNullOrWhiteSpace(model.PartnerName), () =>
			{
				RuleFor(model => model.PartnerName).Must(PartnerNameExists).WithMessage("Le partenaire existe déjà");
			});

            RuleFor(model => model.LogoUrl).NotNull().WithMessage("Le logo du partenaire est requis");
			When(model => model.PartnerId > 0, () =>
			{
				RuleFor(model => model.PartnerToken).NotNull().WithMessage("Le token d'authentification est requis");
			});

            When(model => model.PrestashopWired,() =>{
                RuleFor(model => model.PrestashopModuleUrl).NotNull().WithMessage("L'url du module Prestashop est requis");
            });

            RuleFor(model => model.WarehouseList).SetCollectionValidator(new WarehouseViewModelValidator());
        }

		/// <summary>
		/// Partners the name exists.
		/// </summary>
		/// <returns><c>true</c>, if name exists was partnered, <c>false</c> otherwise.</returns>
		/// <param name="partnerName">Partner name.</param>
		private bool PartnerNameExists(string partnerName)
		{
			return !_service.PartnerExist(partnerName);
		}
    }
}