namespace AspNetCoreReactJs.Validators{

    using Models;
    using FluentValidation;

	/// <summary>
	/// Warehouse view model validator.
	/// </summary>
    public class WarehouseViewModelValidator:AbstractValidator<WarehouseViewModel>{

		/// <summary>
		/// Initializes a new instance of the <see cref="T:AspNetCoreReactJs.Validators.WarehouseViewModelValidator"/> class.
		/// </summary>
        public WarehouseViewModelValidator(){
            RuleFor(model => model.CategoryIdList).
            Cascade(CascadeMode.StopOnFirstFailure).
            NotNull().
            Must(p => p.Count > 0).
            WithMessage("CategoryIdList");
        }
    }
}